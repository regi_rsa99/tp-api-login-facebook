const express = require('express');
const app = express();
const cors = require('cors');
app.use(cors());

app.get('/',(req, res)=>{
    console.log("Chamado GET!");
    res.send("Method GET - Route: /");
});


app.get('/PesUser', (req, res)=>{
    console.log("Consultando Usuario...");
    console.log(req.query);
    let retorno;
    retorno = "<h1>Seja Bem Vindo Ao Facebook</h1>"
    res.send(retorno);
});

app.post('/InsUser', (req, res)=>{
    console.log("Cadastrando Usuario...");
    console.log(req.query);
    let retorno;
    retorno = "<h1>Usuario Cadastrado Com Sucesso!</h1>"
    res.send(retorno);
    
});

app.listen(8080, function(){
    console.log("API Iniciado na Porta: http://localhost:8080");
})
