const{createServer} = require('http');

let server = createServer(
    (request,response) => {
        response.writeHead(200, {"content-type":"text/html"});
        response.write("<h1>Hello Word</h1>");
        response.end();
    }
);

server.listen(8080);

console.log("Projeto inicado na porta 8080");